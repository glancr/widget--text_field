module TextField
  class Engine < ::Rails::Engine
    isolate_namespace TextField
    config.generators.api_only = true

    DEFAULT_STYLES = { font_size: 200, horizontal_align: 'center' }.freeze
  end
end
